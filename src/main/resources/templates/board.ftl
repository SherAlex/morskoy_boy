<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Table</title>
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Alexander & Helen">
    <link rel="stylesheet" href="/webroot/css/foundation.css">
    <link rel="stylesheet" href="/webroot/css/style.css">
</head>
<body>
<div class="row">
    <h5>Морской бой</h5>
    <p>Два игрока теперь могут играть морской бой на зачёте</p>
    <#assign direction = "W">
    <a href="/${direction}" class="radius medium success button"> W </a>
    <#assign direction = "E">
    <a href="/${direction}" class="radius medium success button"> E </a>
    <#assign direction = "N">
    <a href="/${direction}" class="radius medium success button"> N </a>
    <#assign direction = "S">
    <a href="/${direction}" class="radius medium success button"> S </a><br>

    <#assign size = 4>
    <a href="/${size}" class="radius medium success button"> 4 </a>
    <#assign size = 3>
    <a href="/${size}" class="radius medium success button"> 3 </a>
    <#assign size = 2>
    <a href="/${size}" class="radius medium success button"> 2 </a>
    <#assign size = 1>
    <a href="/${size}" class="radius medium success button"> 1 </a><br>

    <#assign setShip = 1>
    <a href="/${setShip}" class="radius medium success button"> setShip </a><br>

    <#assign done = 1>
    <a href="/${done}/" class="radius medium success button"> done </a><br>

    <#assign x = -1>
    <#list tableArray as innerArray>
        <#assign x = x+1>
        <div class="large-1 columns">
            <#assign y = -1>
            <#list innerArray as itemValue>
                <#assign y = y+1>
                    <a href="/${x}/${y}" class="radius medium secondary button">~${itemValue}~</a><br>
            </#list>
        </div>
    </#list>
</div>
</body>
</html>