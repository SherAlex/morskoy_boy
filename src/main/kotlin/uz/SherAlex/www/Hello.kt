package uz.SherAlex.www

import java.math.BigDecimal

fun main() {
    println("Hello, world")

    val stroka = readLine()
    println("Stroka iz console: $stroka")
    //val console = readLine()

    //var console = readLine()
    //val (x,y,dir) = console!!.split(" ")

    var console = readLine()
    val(a,b) = console!!.split(" ")

    var bdA = BigDecimal(a)
    var bdB = BigDecimal(b)
    println("$bdA + $bdB = ${bdA +bdB}")

}