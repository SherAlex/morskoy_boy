package uz.SherAlex.www

import io.vertx.ext.web.client.WebClient

class MoyClient
{
    class MoyClient : io.vertx.core.AbstractVerticle()
    {
        override fun start() {
            var client = WebClient.create(vertx)

            client.get(8080, "127.0.0.1", "/").send { ar ->
                if (ar.succeeded()) {
                    var response = ar.result()
                    println("GOT HTTP response with status ${response.statusCode()} with data ${response.body().toString("ISO-8859-1")}")
                } else {
                    ar.cause().printStackTrace()
                }
            }
        }
    }
}