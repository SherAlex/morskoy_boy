package uz.SherAlex.www.controller

import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.templ.freemarker.FreeMarkerTemplateEngine
import uz.SherAlex.www.Board
import uz.SherAlex.www.Cell
import uz.SherAlex.www.ServerHtml as ServerHtml

class BoardController(vertx: Vertx)
{
    val engine = FreeMarkerTemplateEngine.create(vertx)
    val boardEnemy1 = makeBoardEnemy()
    val boardEnemy2 = makeBoardEnemy()

    var shipCounter = 0

    var board1 = Board(10)
    var board2 = Board(10)

    var dir = ""
    var size = -1
    var c1 = 0
    var c2 = 0

   var FirstDone = false

    fun indexHandler1(ctx: RoutingContext)
    {
        var x = ctx.request().params().get("x")
        var y = ctx.request().params().get("y")
        var data = JsonObject()
        data.put("tableArray", boardEnemy1)
        engine.render(data, "templates/board.ftl")
        {
            if(it.succeeded()) {
                ctx.response().end(it.result())
            } else {
                ctx.response().end(it.cause().message)
            }
        }
        ctx.response().end("Privet Lena")
    }

    fun indexHandler2(ctx: RoutingContext)
    {
        var x = ctx.request().params().get("x")
        var y = ctx.request().params().get("y")
        var data = JsonObject()
        data.put("tableArray", boardEnemy2)
        engine.render(data, "templates/board.ftl")
        {
            if(it.succeeded()) {
                ctx.response().end(it.result())
            } else {
                ctx.response().end(it.cause().message)
            }
        }
        ctx.response().end("Privet Lena")
    }

    fun coordinateHandler1(ctx: RoutingContext)
    {
        var x = ctx.request().params().get("x").toInt()
        var y = ctx.request().params().get("y").toInt()
        boardEnemy1.getJsonArray(x).set(y, "*")
        c1 = x
        c2 = y
        indexHandler1(ctx)
    }
    fun coordinateHandler2(ctx: RoutingContext)
    {
        var x = ctx.request().params().get("x").toInt()
        var y = ctx.request().params().get("y").toInt()
        boardEnemy1.getJsonArray(x).set(y, "*")
        c1 = x
        c2 = y
        indexHandler2(ctx)
    }

    fun getDirection(ctx: RoutingContext)
    {
        dir = ctx.request().params().get("direction").toString()
    }

    fun getSize(ctx: RoutingContext)
    {
        size = ctx.request().params().get("size").toInt()
    }

    fun addShip1(ctx: RoutingContext)
    {
        if(board1.addShip(size.toInt(), Cell(c1.toInt(),c2.toInt()),dir))
        {
            println("Your ship has successfully placed")
            shipCounter++
            println("Current number of ships: $shipCounter")
        }
        else {
            println("You can't put the ship on this place")
        }
        if(shipCounter == 10)
        {
            FirstDone = true
        }
        indexHandler1(ctx)
    }

    fun addShip2(ctx: RoutingContext)
    {
        if(board2.addShip(size.toInt(), Cell(c1.toInt(),c2.toInt()),dir))
        {
            println("Your ship has successfully placed")
            shipCounter++
            println("Current number of ships: $shipCounter")
        }
        else {
            println("You can't put the ship on this place")
        }
        if(shipCounter == 10)
        {
            FirstDone = true
        }
        indexHandler2(ctx)
    }

    fun makeBoardEnemy(): JsonArray
    {
        var tableArray = JsonArray()
        for(k in 0..9)
        {
            var tableInner = JsonArray()
            for (h in 0..9)
            {
                tableInner.add(h+k)
            }
            tableArray.add(tableInner)
        }
        return tableArray
    }
}