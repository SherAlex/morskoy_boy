package uz.SherAlex.www

import io.vertx.core.Vertx
import io.vertx.ext.web.client.WebClient

fun main() {

//    val vertx = Vertx.vertx()
//    vertx.deployVerticle(Server())
//
//    var canWork = true
//    var client = WebClient.create(vertx)
//    while(canWork)
//    {
//
//    }

    var board = Board(10)
    println("Please enter your ships")
    var shipCounter = 0
    while(true) {
        var console = readLine()
        val(size,c1,c2,dir) = console!!.split(" ")
        if(board.addShip(size.toInt(),Cell(c1.toInt(),c2.toInt()),dir)) {
            println("Your ship has successfully placed")
            shipCounter++
            println("Current number of ships: $shipCounter")
        }
        else {
            println("You can't put the ship on this place")
        }
        if(shipCounter == 10) {
            break
        }
    }
    println("The game has started!")
    while(true) {
        var console = readLine()
        val (x,y) = console!!.split(" ")
        println(board.fire(Cell(x.toInt(),y.toInt())).status)
    }

}