package uz.SherAlex.www

import io.vertx.core.AbstractVerticle
import io.vertx.core.http.HttpServerRequest
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.StaticHandler
import uz.SherAlex.www.controller.BoardController

class ServerHtml : AbstractVerticle()
{
//    var dir = ""
//    var size = "-1"
//    var c1 = 0
//    var c2 = 0

    override fun start()
    {
        val BoardController = BoardController(vertx)

        var router = Router.router(vertx)

        router.route("/").handler(BoardController::indexHandler1)

        var shipCounter = 0

        while(true)
        {
            router.route("/size").handler(BoardController::getSize)
            router.route("/direction").handler(BoardController::getDirection)
            router.route("/:x/:y").handler(BoardController::coordinateHandler1)
            router.route("/setShip").handler(BoardController::addShip1)
            shipCounter++;
            if(shipCounter == 10)
            {
                break
            }
        }
        router.route("/done").handler(BoardController::indexHandler2)
        while(true)
        {
            router.route("/size").handler(BoardController::getSize)
            router.route("/direction").handler(BoardController::getDirection)
            router.route("/:x/:y").handler(BoardController::coordinateHandler2)
            router.route("/setShip").handler(BoardController::addShip2)
            shipCounter++;
            if(shipCounter == 10)
            {
                break
            }
        }

        vertx.createHttpServer()
            .requestHandler(router)
            .listen(80)
    }
}
