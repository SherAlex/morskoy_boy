package uz.SherAlex.www

import io.vertx.core.AbstractVerticle
import io.vertx.core.http.HttpServerRequest

class Server : AbstractVerticle()
{
    override fun start() {
        vertx.createHttpServer()
            .requestHandler { req: HttpServerRequest ->
                val a = req.getParam("a")
                val b = req.getParam("b")
                req.response()
                    .putHeader("content-type", "text/plain")
                    .end("Hello from Sasha: $a + $b = ${a.toInt() + b.toInt()}")
            }.listen(80)
    }
}